// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GDS4_ProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GDS4_PROJECT_API AGDS4_ProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

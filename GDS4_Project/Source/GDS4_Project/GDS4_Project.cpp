// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "GDS4_Project.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GDS4_Project, "GDS4_Project" );
